
import os
# two types of files in Python
# Text
# Binary

# 1. create/writing a file
filename = "output.txt"
fd = open(filename, 'w')
fd.write("quantom is the institute\n")
fd.write("DevOps training class")
fd.close()

# Read the file
mystr = ""
fd = open(filename, 'r')
while True:
    ch = fd.read(100)
    if not ch:
        break
    else:
        mystr += ch
fd.close()
print("Data read from file is ")
print(mystr)

