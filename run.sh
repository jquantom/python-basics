argc=$#

if [ $argc -lt 1 ]
then
  echo "Not enough arguments."
  echo "Need atleast one argument as python file, should end with .py"
  echo "Usage: run.sh <python-file>"
  exit 1
fi

arg1=$1

# Checking if file exists
if [ -f $arg1 ]
then
  echo "executing $arg1...."
  python $arg1
  echo "Executiong completed "
fi
