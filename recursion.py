# Infinite loop
#while True:
#    print('quantom.......')

# Recursion  (myrec is a recursive function)
def myrec(val):
    print(f"val = {val}")

    if val <= 1:
        return 0
    myrec(val - 1)

# How do I print 1 - 100 without actually printing 1...100
#myrec(100)


def myrec_rev(val):
    if val <= 1:
        return 0
    myrec_rev(val - 1)
    print(f"val = {val}")

# Lets print the ascii value of 100
ascii_char = chr(100)
print(f"The ascii char is {ascii_char}")

# How do I print 1 - 100 without actually printing 1...100
#myrec(100)
#myrec_rev(100)
myval = ord(ascii_char)
#myrec_rev(ord('d'))  # ascii value of d is 100
#myrec_rev(myval)


# Fibonacci series
# 0 1 1 2 3 5 8 13 21 ...

def fibo(n):
    if n == 0 or n == 1:
        return n
    else:
        return (fibo(n-1) + fibo(n - 2))


for val in range(5):
    print(fibo(val))

# same problem (fibonacci) with for/while loop

# Factorial (recursion)
# n = 3
# n! = 3 X 2 X 1
def fact(n):
    if n <= 1:
        return 1
    else:
        return n * fact(n - 1)

mynum = 5
myfact = fact(mynum)
print(f"The factorial of {mynum} is {myfact}")
# do the problem(factorial) with for / while loop

# Binary Search Tree
# Trying to find 38  Order of nlog(n)
#0
#...
#100

# variable arguments
# keyword variable arguments
# Try/Catch : Exceptions
# File operations
# inheritance / abstract classes
# date and time
# Decorators
# Generators
# Iterators
# pytest
# Mongodb
# FastAPI
# Queues
